
SELECT
    volume_mount_point AS Volume,
    CAST(MIN(VolumeTotalMo) AS DECIMAL(18, 2)) AS VolumeSpaceMo,
    CAST(SUM(FileSizeMo) AS DECIMAL(18, 2)) AS DBFilesSpaceMo,
    CAST(MIN(VolumeTotalMo)-SUM(FileSizeMo)-MIN(VolumeAvailableMo) AS DECIMAL(18,2)) AS OtherMo,
    CAST(MIN(VolumeAvailableMo) AS DECIMAL(18, 2)) AS VolumeFreeSpaceMo,
    CAST(SUM(FileGrowthMo) AS DECIMAL(18, 2)) AS DBGrowthSpaceMo,
    CAST(MIN(VolumeAvailableMo) - SUM(FileGrowthMo) AS DECIMAL(18, 2)) AS FreeSpaceAfterGrowthMo,
    CAST(MIN(VolumeAvailableMo) / MIN(VolumeTotalMo) * 100 AS DECIMAL(6, 2)) AS [Pct_VolumeFree],
    CAST(( MIN(VolumeAvailableMo) - SUM(FileGrowthMo) )
       / MIN(VolumeTotalMo) * 100 AS DECIMAL(6, 2)) AS [Pct_FreeAfterGrowth]
FROM
    (
        SELECT
            DB_NAME(f.database_id) AS [DatabaseName],
            f.file_id,
            CAST(f.size AS FLOAT) * 8 / 1024 AS FileSizeMo,
            CASE
                WHEN f.is_percent_growth = 1
                THEN ( CAST(f.size AS FLOAT) * f.growth / 100 ) * 8 / 1024
                ELSE CAST(f.growth AS FLOAT) * 8 / 1024
            END AS FileGrowthMo,
            vs.volume_mount_point,
            CAST(vs.total_bytes AS FLOAT) / 1024 / 1024 AS VolumeTotalMo,
            CAST(vs.available_bytes AS FLOAT) / 1024 / 1024 AS VolumeAvailableMo,
            ( CAST(vs.available_bytes AS FLOAT)
              / CAST(vs.total_bytes AS FLOAT) ) * 100 AS [Space Free %]
        FROM sys.master_files AS f
        CROSS APPLY sys.dm_os_volume_stats(f.database_id, f.file_id) AS vs
    ) AS FileSizes
GROUP BY
    volume_mount_point
ORDER BY
    volume_mount_point;

