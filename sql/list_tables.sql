
-- databases
SELECT
    database_id,
    name db_name,
    create_date
FROM sys.databases;

-- tables
SELECT
    t.object_id,
    s.name schema_name,
    t.name table_name,
    t.max_column_id_used,
    t.create_date,
    t.modify_date
FROM sys.tables t
JOIN sys.schemas s ON t.schema_id = s.schema_id
ORDER BY
    schema_name,
    table_name;

-- views
SELECT
    v.object_id,
    s.name schema_name,
    v.name table_name,
    v.create_date,
    v.modify_date
FROM sys.views v
JOIN sys.schemas s ON v.schema_id = s.schema_id
ORDER BY
    schema_name,
    table_name;

-- columns
SELECT
    c.object_id,
    sc.name,
    o.name table_name,
    c.column_id,
    c.name col_name,
    su.name typ_usr_name,
    sty.name typ_sys_name,
    c.max_length,
    c.precision,
    c.scale,
    c.is_nullable,
    c.collation_name
FROM sys.all_columns c
JOIN sys.objects o ON c.object_id = o.object_id
JOIN sys.types sty ON c.system_type_id = sty.user_type_id
JOIN sys.types su ON c.user_type_id = su.user_type_id
JOIN sys.schemas sc ON o.schema_id = sc.schema_id
WHERE
    o.type IN('U','V')
ORDER BY
    table_name,
    column_id;

-- primary key
SELECT
    i.object_id,
    i.name index_name,
    s.name table_schema,
    t.name table_name,
    c.column_id,
    c.name column_name,
    c.is_identity
FROM sys.indexes i
JOIN sys.index_columns ic  ON i.object_id = ic.object_id AND i.index_id = ic.index_id
JOIN sys.columns c ON ic.object_id = c.object_id AND c.column_id = ic.column_id
JOIN sys.objects t ON c.object_id = t.object_id
JOIN sys.schemas s ON t.schema_id = s.schema_id
WHERE
    i.is_primary_key = 1;

-- forein key
SELECT
    fk.object_id,
    fk.name foreign_key_name,
    ps.name table_schema,
    pt.name table_name,
    pc.name column_name,
    rs.name referenced_table_schema,
    rt.name referenced_table_name,
    rc.name referenced_column_name
FROM sys.schemas ps
JOIN  sys.foreign_keys fk ON ps.schema_id = fk.schema_id
JOIN  sys.foreign_key_columns fkc ON fk.object_id = fkc.constraint_object_id
JOIN  sys.tables pt ON fk.parent_object_id = pt.object_id
JOIN  sys.columns pc ON fkc.parent_object_id = pc.object_id AND fkc.parent_column_id = pc.column_id
JOIN  sys.tables rt ON fk.referenced_object_id = rt.object_id
JOIN  sys.columns rc ON fkc.referenced_object_id = rc.object_id AND fkc.referenced_column_id = rc.column_id
JOIN  sys.schemas rs ON rt.schema_id = rs.schema_id
ORDER BY
    table_name,
    column_name;


