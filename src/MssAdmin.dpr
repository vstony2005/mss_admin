program MssAdmin;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  fMain in 'fMain.pas' {FrmMain},
  uServer in 'uServer.pas',
  uOptions in 'uOptions.pas',
  mDatas in 'mDatas.pas' {DataModule1: TDataModule1},
  fFormForTab in 'fFormForTab.pas' {FrmForTab},
  fInfosDbs in 'fInfosDbs.pas' {FrmInfosDbs};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDataModule1, MyDatas);
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
