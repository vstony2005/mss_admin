unit fAddServer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Vcl.Buttons;

type
  TFrmAddServer = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtServer: TEdit;
    edtUser: TEdit;
    edtPassword: TEdit;
    btnCancel: TSpeedButton;
    btnOk: TSpeedButton;
    btnShow: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnShowClick(Sender: TObject);
  private
    function GetServer: string;
    function GetUser: string;
    function GetPassword: string;
  public
    property Server: string read GetServer;
    property User: string read GetUser;
    property Password: string read GetPassword;
  end;

implementation

{$R *.dfm}

uses
  System.UITypes;

procedure TFrmAddServer.FormCreate(Sender: TObject);
begin
  edtPassword.PasswordChar := '*';
end;

procedure TFrmAddServer.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFrmAddServer.btnOkClick(Sender: TObject);
var
  isCancel: Boolean;
begin
  isCancel := False;
  if (Trim(edtServer.Text) = '') then
  begin
    if (MessageDlg('Server is not set. Would you like to add it?'
                   + #13#10'If you cancel the input, the data will be lost.',
                   TMsgDlgType.mtConfirmation,
                   [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) = mrYes) then
    begin
      if (edtServer.CanFocus) then edtServer.SetFocus;
      Abort;
    end;
    isCancel := True;
  end
  else if (Trim(edtUser.Text) = '') then
  begin
    if (MessageDlg('User is not set. Would you like to add it?'
                   + #13#10
                   + #13#10'If you cancel the input, the data will be lost.',
                   TMsgDlgType.mtConfirmation,
                   [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) = mrYes) then
    begin
      if (edtUser.CanFocus) then edtUser.SetFocus;
      Abort;
    end;
    isCancel := True;
  end
  else if (Trim(edtPassword.Text) = '') then
  begin
    if (MessageDlg('Password is not set. Would you like to add it?'
                   + #13#10'If you cancel the input, the data will be lost.',
                   TMsgDlgType.mtConfirmation,
                   [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) = mrYes) then
    begin
      if (edtPassword.CanFocus) then edtPassword.SetFocus;
      Abort;
    end;
    isCancel := True;
  end;

  if (isCancel) then
    ModalResult := mrCancel
  else
    ModalResult := mrOk;
end;

procedure TFrmAddServer.btnShowClick(Sender: TObject);
begin
  if (edtPassword.PasswordChar = '*') then
  begin
    edtPassword.PasswordChar := #0;
    btnShow.Caption := '&Hide';
  end
  else
  begin
    edtPassword.PasswordChar := '*';
    btnShow.Caption := '&Show';
  end;
end;

function TFrmAddServer.GetServer: string;
begin
  Result := edtServer.Text;
end;

function TFrmAddServer.GetUser: string;
begin
  Result := edtUser.Text;
end;  

function TFrmAddServer.GetPassword: string;
begin
  Result := edtPassword.Text;
end;

end.
