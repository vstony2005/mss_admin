unit fFormForTab;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, uServer;

type
  TFrmForTab = class(TForm)
  private
    procedure SetServer(const AServer: TServer);
  protected
    FServer: TServer;
  public
    class function CreateForTabSheet(const ATab: TTabSheet; AServer: TServer): TFrmForTab;
    property Server: TServer write SetServer;
  end;

implementation

{$R *.dfm}

{ TFrmForTab }

class function TFrmForTab.CreateForTabSheet(const ATab: TTabSheet; AServer:
    TServer): TFrmForTab;
var
  frm: TFrmForTab;
begin
  Result := nil;
  frm := Self.Create(ATab);
  try
    Result := frm;
    frm.Server := AServer;
    frm.BorderStyle := bsNone;
    frm.Parent := ATab;
    frm.Align := alClient;
    frm.Show;
  finally
  end;
end;

procedure TFrmForTab.SetServer(const AServer: TServer);
begin
  FServer := AServer;
end;

end.
