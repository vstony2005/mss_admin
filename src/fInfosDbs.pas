unit fInfosDbs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Comp.Client,
  Vcl.ComCtrls,
  uServer, fFormForTab;

type
  TFrmInfosDbs = class(TFrmForTab)
    grid1: TDBGrid;
    ds1: TDataSource;
    cds1: TClientDataSet;
    dsp1: TDataSetProvider;
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FQry: TFDQuery;
  protected
    procedure SetField(ADataSet: TDataSet; const AFieldName: string;
        AWidth: Integer; const ALabel: string = '');
    function GetFormatNumeric: string; virtual;
    function GetFormatDatetime: string; virtual;
    function GetSqlText: string; virtual; Abstract;
  public
  end;

implementation

{$R *.dfm}

uses
  mDatas,
  FireDAC.DApt;

{ TFrmInfosDbs }

procedure TFrmInfosDbs.FormShow(Sender: TObject);
begin
  if Assigned(FServer) then
  begin
    FQry := MyDatas.GetQuery;
    if Assigned(FQry) then
    begin
      FQry.SQL.Text := GetSqlText;
      dsp1.DataSet := FQry;
      cds1.Open;
    end;
  end;
end;

procedure TFrmInfosDbs.FormDestroy(Sender: TObject);
begin
  FQry.Free;
end;

function TFrmInfosDbs.GetFormatNumeric: string;
begin
  Result := '0.00';
end;

function TFrmInfosDbs.GetFormatDatetime: string;
begin
  Result := 'dd/mm/yyyy';
end;

procedure TFrmInfosDbs.SetField(ADataSet: TDataSet; const AFieldName: string;
  AWidth: Integer; const ALabel: string);
var
  field: TField;
begin
  field := ADataSet.FieldByName(AFieldName);
  field.DisplayWidth := AWidth;

  if (ALabel <> '') then
    field.DisplayLabel := ALabel;

  if (field.DataType in [ftFloat, ftFMTBcd]) then
    TNumericField(field).DisplayFormat := GetFormatNumeric
  else if (field.DataType = ftTimeStamp) then
    TSQLTimeStampField(field).DisplayFormat := GetFormatDatetime;
end;

end.
