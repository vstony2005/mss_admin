unit fInfosDbsFiles;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, fInfosDbs, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids;

type
  TFrmInfosDbsFiles = class(TFrmInfosDbs)
    procedure cds1AfterOpen(DataSet: TDataSet);
  private
  protected
    function GetSqlText: string; override;
  public
  end;

implementation

{$R *.dfm}

procedure TFrmInfosDbsFiles.cds1AfterOpen(DataSet: TDataSet);
begin
  inherited;

  SetField(DataSet, 'destination_database_name', 25, 'Name');
  SetField(DataSet, 'restore_date', 10, 'Date Restore');
  SetField(DataSet, 'source_database_name', 25, 'Source');
  SetField(DataSet, 'backup_start_date', 10, 'Backup start');
  SetField(DataSet, 'backup_finish_date', 10, 'Backup finish');
  SetField(DataSet, 'backup_file_used_for_restore', 65, 'File restore');
end;

{ TFrmInfosDbs1 }

function TFrmInfosDbsFiles.GetSqlText: string;
begin
  Result := 'SELECT '
    + '    rs.destination_database_name, '
    + '    rs.restore_date, '
    + '    bs.database_name AS source_database_name, '
    + '    bs.backup_start_date, '
    + '    bs.backup_finish_date, '
    + '    bmf.physical_device_name AS backup_file_used_for_restore '
    + 'FROM msdb..restorehistory rs '
    + 'JOIN msdb..backupset bs ON rs.backup_set_id = bs.backup_set_id '
    + 'JOIN msdb..backupmediafamily bmf ON bs.media_set_id = bmf.media_set_id '
    + 'ORDER BY '
    + '    rs.restore_date DESC;';
end;

end.
