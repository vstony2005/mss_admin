unit fInfosDbsNamesFiles;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, fInfosDbs, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids;

type
  TFrmInfosDbsNameFiles = class(TFrmInfosDbs)
    procedure cds1AfterOpen(DataSet: TDataSet);
  private
  protected
    function GetSqlText: string; override;
  end;

var
  FrmInfosDbsNameFiles: TFrmInfosDbsNameFiles;

implementation

{$R *.dfm}

procedure TFrmInfosDbsNameFiles.cds1AfterOpen(DataSet: TDataSet);
begin
  inherited;

  SetField(DataSet, 'db_name', 25, 'Database');
  SetField(DataSet, 'logical_name', 25, 'Logical name');
  SetField(DataSet, 'filename', 100, 'File');
end;

{ TFrmInfosDbsNameFiles }

function TFrmInfosDbsNameFiles.GetSqlText: string;
begin
  Result := 'SELECT '
            + '    a.name db_name,'
            + '    b.name logical_name,'
            + '    b.filename '
            + 'FROM sys.sysdatabases a '
            + 'JOIN sys.sysaltfiles b on b.dbid = a.dbid;';
end;

end.
