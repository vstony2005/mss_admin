unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  uOptions, MidasLib, Vcl.Buttons,
  uServer,
  fRestore,
  fInfosDbsSize,
  fInfosDbsFiles,
  fInfosDbsNamesFiles,
  fScriptSql;

type
  TFrmMain = class(TForm)
    lblServers: TLabel;
    cbxSrv: TComboBox;
    lblServer: TLabel;
    lblConnect: TLabel;
    pgcInfos: TPageControl;
    tsSize: TTabSheet;
    tsFiles: TTabSheet;
    tsNameFiles: TTabSheet;
    btnAddServer: TSpeedButton;
    btnEdit: TSpeedButton;
    btnConnect: TSpeedButton;
    pgc1: TPageControl;
    tsRestore: TTabSheet;
    tsInfos: TTabSheet;
    pgcRestore: TPageControl;
    tsGeneral: TTabSheet;
    tsScriptSql: TTabSheet;
    btnDisconnect: TSpeedButton;
    procedure btnConnectClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cbxSrvChange(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnAddServerClick(Sender: TObject);
    procedure btnDisconnectClick(Sender: TObject);
  private
    FOptions: TOptions;
    FTabRestore: TfrmRestore;
    FTabScript: TFrmScriptSql;
    FTabSize: TFrmInfosDbsSize;
    FTabFiles: TFrmInfosDbsFiles;
    FTabName: TFrmInfosDbsNameFiles;
    procedure LoadDatas(AIsReadFile: Boolean);
    procedure SaveDatas;
    procedure AddServer(const AName, AUser, APassword: string);
    procedure EventRestore(Sender: TObject);
    procedure EventSaveBackupPath(Sender: TObject);
    procedure OpenTabs(const srv: TServer);
    procedure CloseTabs;
    procedure EnabledBtnConnect(AIsEnabled: Boolean);
    procedure SelectDefaultTabs;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

uses
  fAddServer,
  mDatas,
  fMajServers;

const
  JSON_FILE: string = '.\datas.json';

{$REGION 'Events'}
procedure TFrmMain.FormCreate(Sender: TObject);
begin
  FOptions := TOptions.Create;

  EnabledBtnConnect(True);

  lblServer.Caption := '-';
  lblConnect.Caption := '-';

//  {$DEFINE DB_TEST}

  {$IFDEF DB_TEST}
  AddServer('.', 'sa', 'pass');
  AddServer('.\sql2008', 'sa', 'pass');
  AddServer('.\sql2008r2', 'sa', 'pass');
  AddServer('.\sql2012', 'sa', 'pass');
  AddServer('.\sql2014', 'sa', 'pass');
  AddServer('.\sql2016', 'sa', 'pass');
  AddServer('.\sql2017', 'sa', 'pass');
  AddServer('.\sql2019', 'sa', 'pass');
  AddServer('.\sql2022', 'sa', 'pass');
  {$ELSE}
  LoadDatas(True);
  {$ENDIF}
end;

procedure TFrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  SaveDatas;
end;

procedure TFrmMain.FormDestroy(Sender: TObject);
begin
  FOptions.Free;
end;

procedure TFrmMain.cbxSrvChange(Sender: TObject);
var
  srv: TServer;
begin
  if (cbxSrv.ItemIndex < 0) then
  begin
    lblServer.Caption := '-';
    Exit;
  end;

  srv := TServer(cbxSrv.Items.Objects[cbxSrv.ItemIndex]);
  lblServer.Caption := Format('%s (%s)', [srv.Name, srv.User]);
end;

procedure TFrmMain.EventRestore(Sender: TObject);
var
  str: string;
begin
  if Assigned(FTabRestore)
    and Assigned(FTabScript) then
  begin
    str := FTabRestore.SqlText;
    if (not str.IsEmpty) then
    begin
      FTabScript.RestoreSql := str;
      FTabScript.DbName := FTabRestore.DbName;
      pgcRestore.TabIndex := 1;
    end;
  end;
end;

procedure TFrmMain.EventSaveBackupPath(Sender: TObject);
begin
  if Assigned(FTabRestore) then
    FOptions.BackupPath := FTabRestore.BackupPath;
end;
{$ENDREGION}

{$REGION 'Buttons'}
procedure TFrmMain.btnAddServerClick(Sender: TObject);
var
  frm: TFrmAddServer;
  srv: TServer;
begin
  frm := TFrmAddServer.Create(Self);
  try
    if (frm.ShowModal = mrOk) then
    begin
      if (not frm.Server.Trim.IsEmpty)
        and (not frm.User.Trim.IsEmpty)
        and (not frm.Password.Trim.IsEmpty) then
      begin
        AddServer(frm.Server, frm.User, frm.Password);
        cbxSrv.ItemIndex := cbxSrv.Items.Count-1;
      end;
    end;
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.btnEditClick(Sender: TObject);
var
  frm: TFrmMajServers;
begin
  frm := TFrmMajServers.Create(Self);
  try
    frm.Options := FOptions;
    if (frm.ShowModal = mrOk) then
      LoadDatas(False);
  finally
    frm.Free;
  end;
end;

procedure TFrmMain.btnConnectClick(Sender: TObject);
var
  srv: TServer;
begin
  if (cbxSrv.ItemIndex < 0) then
  begin
    lblConnect.Caption := '-';
    Exit;
  end;

  btnDisconnectClick(btnDisconnect);

  srv := TServer(cbxSrv.Items.Objects[cbxSrv.ItemIndex]);
  if MyDatas.ConnectMaster(srv) then
  begin
    lblConnect.Caption := 'ok';
    OpenTabs(srv);
    EnabledBtnConnect(False);
  end
  else
    lblConnect.Caption := 'nok';
end;

procedure TFrmMain.btnDisconnectClick(Sender: TObject);
begin
  CloseTabs;
  EnabledBtnConnect(True);
end;
{$ENDREGION}

procedure TFrmMain.LoadDatas(AIsReadFile: Boolean);
var
  i: Integer;
  srv: TServer;
begin
  if (AIsReadFile) then
    FOptions.ReadFile(JSON_FILE);
  cbxSrv.Clear;
  for i := 0 to FOptions.CountDb - 1 do
  begin
    srv := FOptions.Servers[i];
    cbxSrv.AddItem(srv.Name, srv);
  end;
end;

procedure TFrmMain.SaveDatas;
begin
  FOptions.WriteFile(JSON_FILE);
end;

procedure TFrmMain.OpenTabs(const srv: TServer);
begin
  FTabRestore := TfrmRestore.CreateForTabSheet(tsGeneral, srv) as TFrmRestore;
  FTabRestore.BackupPath := FOptions.BackupPath;
  FTabRestore.OnGenerateSql := Self.EventRestore;
  FTabRestore.OnSaveBackupPath := Self.EventSaveBackupPath;

  FTabScript := TFrmScriptSql.CreateForTabSheet(tsScriptSql, srv) as TFrmScriptSql;
  FTabSize := TFrmInfosDbsSize.CreateForTabSheet(tsSize, srv) as TFrmInfosDbsSize;
  FTabFiles := TFrmInfosDbsFiles.CreateForTabSheet(tsFiles, srv) as TFrmInfosDbsFiles;
  FTabName := TFrmInfosDbsNameFiles.CreateForTabSheet(tsNameFiles, srv) as TFrmInfosDbsNameFiles;
end;

procedure TFrmMain.CloseTabs;
begin
  if Assigned(FTabRestore) then
  begin
    FTabRestore.Free;
    FTabRestore := nil;
  end;
  if Assigned(FTabScript) then
  begin
    FTabScript.Free;
    FTabScript := nil;
  end;
  if Assigned(FTabSize) then
  begin
    FTabSize.Free;
    FTabSize := nil;
  end;
  if Assigned(FTabFiles) then
  begin
    FTabFiles.Free;
    FTabFiles := nil;
  end;
  if Assigned(FTabName) then
  begin
    FTabName.Free;
    FTabName := nil;
  end;
end;

procedure TFrmMain.EnabledBtnConnect(AIsEnabled: Boolean);
begin
  btnConnect.Enabled := AIsEnabled;
  btnDisconnect.Enabled := not btnConnect.Enabled;
  SelectDefaultTabs;
end;

procedure TFrmMain.SelectDefaultTabs;
begin
  pgc1.TabIndex := 0;
  pgcRestore.TabIndex := 0;
  pgcInfos.TabIndex := 0;
end;

procedure TFrmMain.AddServer(const AName, AUser, APassword: string);
var
  srv: TServer;
begin
  srv := TServer.Create;
  srv.Name := AName;
  srv.User := AUser;
  srv.Password := APassword;

  FOptions.AddServer(srv);
  cbxSrv.AddItem(srv.Name, srv);
end;

end.
