object FrmMajServers: TFrmMajServers
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Edit Servers'
  ClientHeight = 463
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    624
    463)
  TextHeight = 15
  object Label1: TLabel
    Left = 8
    Top = 321
    Width = 32
    Height = 15
    Anchors = [akLeft, akBottom]
    Caption = 'Server'
  end
  object Label2: TLabel
    Left = 8
    Top = 352
    Width = 23
    Height = 15
    Anchors = [akLeft, akBottom]
    Caption = 'User'
  end
  object Label3: TLabel
    Left = 8
    Top = 379
    Width = 50
    Height = 15
    Anchors = [akLeft, akBottom]
    Caption = 'Password'
  end
  object btnOk: TSpeedButton
    Left = 535
    Top = 428
    Width = 75
    Height = 27
    Anchors = [akRight, akBottom]
    Caption = '&OK'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000001FFFFFFFFFFFF
      FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDA3BFB2369D6E008C4B00
      8B4A008B4A008C4B369D6EA3BFB2E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      E1E1E144A27700905001A16901AB7601AC7901AC7901AB7601A16900905055A8
      82E1E1E1FFFFFFFFFFFFFFFFFFF5F5F555A88200915202AC7700C38C00D79B00
      DA9C00DA9C00D79C01C38C01AB7600925355A882F5F5F5FFFFFFFFFFFFAECABD
      0090510FB48300D29800D59800D19200CF9000D09100D39600D69B00D19801AB
      76009050AECABDFFFFFFFFFFFF369D6C16AB7810C99600D39700CD8CFFFFFFFF
      FFFFFFFFFF00CC8C00D19500D59B01C18C01A169369E6EFFFFFFFFFFFF008A48
      39C49D00D19800CB8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CA8C00CF9600D2
      9B01AB76008C4BFFFFFFFFFFFF00894652D2B000CC92FFFFFFFFFFFFFFFFFF00
      C484FFFFFFFFFFFFFFFFFF00C88D00D09A00AD79008B4AFFFFFFFFFFFF008845
      68DDBE00C991FFFFFFFFFFFF00C68C00C89100C58BFFFFFFFFFFFFFFFFFF00CC
      9600AD78008B4AFFFFFFFFFFFF00884676E0C600CB9800C59000C69100C89500
      C99700C89400C38CFFFFFFFFFFFF00C79200AB75008C4BFFFFFFFFFFFF41A675
      59C9A449DEBC00C79400C89700C99800C99900C99800C79400C38EFFFFFF00BD
      8A00A06740A878FFFFFFFFFFFFCCE8DB0A9458ADF8E918D0A700C49500C69700
      C69800C79800C79800C69700C59612B585008F50CCE8DBFFFFFFFFFFFFFFFFFF
      55B185199C63BCFFF75EE4C900C59A00C39600C49700C59A22CAA22FC1960293
      556ABC96FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6ABA940E965974D5B6A0F4E194
      EFDC7CE6CC5ED6B52EB58703915255B288FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFCCE8DA44A87700874400874300874400894644AA7ACCE8DBFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    OnClick = btnOkClick
  end
  object btnCancel: TSpeedButton
    Left = 454
    Top = 428
    Width = 75
    Height = 27
    Anchors = [akRight, akBottom]
    Caption = '&Cancel'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000001FFFFFFFFFFFF
      FFFFFFEDEDEDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEDEDEDFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDED9399C82C3CC02B3BBE2B3ABE2B
      3ABE2B3ABE2B3BBE2C3CC09399C8EDEDEDFFFFFFFFFFFFFFFFFFFFFFFFEDEDED
      969BC92F3EC35F71F9697DFF697CFF697CFF697CFF697DFF5F71F92F3EC3969B
      C9EDEDEDFFFFFFFFFFFFEDEDED969BC92F3EC2586BF65F74FF5D72FE5E72FD5E
      73FD5E72FD5D72FE5F74FF586BF62F3EC2969BC9EDEDEDFFFFFF9399C8303FC2
      5568F3586CFC4E64F94D63F85468F9576BF95468F94D63F84E64F9586CFC5568
      F3303FC29399C8FFFFFF2D3DC05367F2556BFA4960F7FFFFFFFFFFFF3E56F647
      5EF63E56F6FFFFFFFFFFFF4960F7556BFA5166F22D3DC0FFFFFF2B3BBF6276FC
      4D64F64259F4FFFFFFFFFFFFFFFFFF2C46F3FFFFFFFFFFFFFFFFFF4259F44E64
      F65F75FC2C3BBFFFFFFF2A3ABF7386FA495FF3435AF36E80F6FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF6E80F6435AF3495FF36E81FA2B3ABFFFFFFF2939BF8696FB
      425AF14259F1354EF05B70F2FFFFFFFFFFFFFFFFFF5B70F2354EF04259F1435B
      F17D90F92A39BFFFFFFF2737BF9AA8FB3A55EF3953EE2844EDFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF2844ED3953EE3B55EF8E9DFA2838BFFFFFFF2637BF9FABF1
      314CED2B47EBFFFFFFFFFFFFFFFFFF5369EFFFFFFFFFFFFFFFFFFF2C47EB314C
      ED9FABF12737BFFFFFFF2838C19FABF18091F4213EE8FFFFFFFFFFFF5D72EE23
      40E85D72EEFFFFFFFFFFFF213EE88091F49FABF12838C1FFFFFFB4BAE92E3EC3
      97A5EF778AF25B71EE6074EE2643E62C48E72643E66074EE5B71EE778AF297A5
      EF2E3EC3B4BAE9FFFFFFFFFFFFB6BBEA2E3EC295A2EE7688F01E3BE42340E525
      41E52340E51E3BE47688F095A2EE2E3EC2B6BBEAFFFFFFFFFFFFFFFFFFFFFFFF
      B6BBEA2F3DC394A0EFADB9F8ADB8F7ADB9F7ADB8F7ADB9F894A0EF2F3DC3B6BB
      EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B9E9303FC44555CE4454CD43
      54CD4454CD4555CE303FC4B3B9E9FFFFFFFFFFFFFFFFFFFFFFFF}
    OnClick = btnCancelClick
  end
  object btnShow: TSpeedButton
    Left = 370
    Top = 376
    Width = 65
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Show'
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000001FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFE5E5E5CECECECCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9CCCCCC67A1C30979BA0377BA03
      77BA0377BA0377B9398DBDA4BBC8E5E5E5FFFFFFFFFFFFFFFFFFFFFFFFF9F9F9
      B4C2CA1587C9339FDD4FB7F256BEFA56BEFC55BDFA55BBF644ABE51F8CCC66A0
      C3DDDDDDFFFFFFFFFFFFFDFDFDB8C5CC3FA2DBAFD5E3F9F5E9E1AF77B8711DB7
      7423B8711DDEAC75F1F9FDA7DBF84AACE2559EC9DEDEDEFFFFFFCCD1D44EADE3
      E4E2D0FFF2DAD2A774A87531988C61919576978D64A57632CA9B64FFFFFFFFFF
      F782C5E862A9D2DCDCDC6CBCE8EDE0C9FFE9CBFCF0DEA66B21938E661A1D2417
      191E171B1F86825FA1691DFFF9F3FFFCF3FFFCEDEAEDE87FBADB0479BC5C9EBC
      FFEDCBFFF2DEA670298C967A18191F1B1C1E5B5B5EA2A799BB9562F0DFCBFFFA
      EEFFFEE95FA4C70277BACFE5F20072B95A9EBFFFF7DAB270219A8E631C1D2014
      131699999AD0CBB8DBBA92FFFFF2FFF5E55CA2C60073B8E3F0F8FFFFFF67AEDB
      0074B91683C28B8568BE7B29AB8C519F8F5FEDD7BAFFE3C6E5DDCE94BFD1137D
      BA0278BDAFD4ECFFFFFFFFFFFFFFFFFF6CB3E01583CA047CC60077C20077C200
      76BF0072BB006FB70073BA057AC01986CBC3E0F2FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFD4E9F662AEE02A92D72B92D72B92D72A91D62A91D63094D692C7E9FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    OnClick = btnShow3Click
  end
  object grid1: TDBGrid
    Left = 8
    Top = 8
    Width = 602
    Height = 289
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = ds1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
  end
  object nav1: TDBNavigator
    Left = 370
    Top = 316
    Width = 240
    Height = 25
    DataSource = ds1
    Anchors = [akRight, akBottom]
    TabOrder = 4
  end
  object edtServer: TDBEdit
    Left = 72
    Top = 318
    Width = 280
    Height = 23
    Anchors = [akLeft, akRight, akBottom]
    DataField = 'server'
    DataSource = ds1
    TabOrder = 1
  end
  object edtPassword: TDBEdit
    Left = 72
    Top = 376
    Width = 280
    Height = 23
    Anchors = [akLeft, akRight, akBottom]
    DataField = 'password'
    DataSource = ds1
    PasswordChar = '*'
    TabOrder = 3
  end
  object edtUser: TDBEdit
    Left = 72
    Top = 347
    Width = 280
    Height = 23
    Anchors = [akLeft, akRight, akBottom]
    DataField = 'user'
    DataSource = ds1
    TabOrder = 2
  end
  object ds1: TDataSource
    DataSet = cds1
    Left = 104
    Top = 408
  end
  object cds1: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cds1AfterInsert
    Left = 136
    Top = 408
    object cds1server: TStringField
      DisplayLabel = 'Server'
      DisplayWidth = 50
      FieldName = 'server'
      Size = 80
    end
    object cds1user: TStringField
      DisplayLabel = 'User'
      DisplayWidth = 40
      FieldName = 'user'
      Size = 45
    end
    object cds1password: TStringField
      DisplayLabel = 'Password'
      DisplayWidth = 25
      FieldName = 'password'
      Visible = False
      Size = 80
    end
  end
end
