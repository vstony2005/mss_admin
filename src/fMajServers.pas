unit fMajServers;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.Grids, Vcl.DBGrids, Datasnap.DBClient, Vcl.StdCtrls, Vcl.Mask, Vcl.Buttons,
  uOptions;

type
  TFrmMajServers = class(TForm)
    grid1: TDBGrid;
    nav1: TDBNavigator;
    ds1: TDataSource;
    cds1: TClientDataSet;
    cds1server: TStringField;
    cds1user: TStringField;
    cds1password: TStringField;
    edtServer: TDBEdit;
    edtPassword: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtUser: TDBEdit;
    btnOk: TSpeedButton;
    btnCancel: TSpeedButton;
    btnShow: TSpeedButton;
    procedure btnCancelClick(Sender: TObject);
    procedure cds1AfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnShow3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FOptions: TOptions;
    procedure CreateCds;
  public
    property Options: TOptions read FOptions write FOptions;
  end;

implementation

{$R *.dfm}

uses
  uServer;

procedure TFrmMajServers.FormCreate(Sender: TObject);
begin
  CreateCds;
end;

procedure TFrmMajServers.FormShow(Sender: TObject);
var
  i: Integer;
begin
  // load datas
  if Assigned(FOptions) then
  begin
    for i := 0 to FOptions.CountDb - 1 do
    begin
      cds1.Append;
      cds1['server'] := FOptions.Servers[i].Name;
      cds1['user'] := FOptions.Servers[i].User;
      cds1['password'] := FOptions.Servers[i].Password;
      cds1.Post;
    end;
  end;
end;

procedure TFrmMajServers.btnOkClick(Sender: TObject);
var
  srv: TServer;
begin
  // save datas
  FOptions.ClearServers;
  cds1.First;
  while (not cds1.Eof) do
  begin
    srv := TServer.Create;
    srv.Name := cds1['server'];
    srv.User := cds1['user'];
    srv.Password := cds1['password'];
    FOptions.AddServer(srv);

    cds1.Next;
  end;
  ModalResult := mrOk;
end;

procedure TFrmMajServers.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFrmMajServers.btnShow3Click(Sender: TObject);
begin
  if (edtPassword.PasswordChar = '*') then
  begin
    edtPassword.PasswordChar := #0;
    btnShow.Caption := '&Hide';
  end
  else
  begin
    edtPassword.PasswordChar := '*';
    btnShow.Caption := '&Show';
  end;
end;

procedure TFrmMajServers.cds1AfterInsert(DataSet: TDataSet);
begin
  if (edtServer.CanFocus) then
    edtServer.SetFocus;
end;

procedure TFrmMajServers.CreateCds;
begin
  try
    cds1.CreateDataSet;
    cds1.LogChanges := False;
    cds1.Open;
  except
    on e:Exception do
      ShowMessage(e.Message);
  end;
end;

end.
