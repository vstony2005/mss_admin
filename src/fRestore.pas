unit fRestore;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  fFormForTab, Vcl.Buttons, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Controls, Data.DB,
  Datasnap.DBClient, Vcl.Mask, Vcl.ExtCtrls, Vcl.DBCtrls, Datasnap.Provider,
  Vcl.Grids, Vcl.DBGrids;

type
  TFrmRestore = class(TFrmForTab)
    openDlg: TOpenDialog;
    edtFile: TEdit;
    Label3: TLabel;
    btnOpenBak: TSpeedButton;
    Label4: TLabel;
    edtLogPath: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    edtBackupPath: TDBEdit;
    edtDataPath: TDBEdit;
    Label8: TLabel;
    ds1: TDataSource;
    cds1: TClientDataSet;
    dsp1: TDataSetProvider;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    dsBak: TDataSource;
    cdsBak: TClientDataSet;
    dspBak: TDataSetProvider;
    edtDbName: TEdit;
    btnValidate: TSpeedButton;
    chkShrink: TCheckBox;
    btnSave: TSpeedButton;
    procedure btnValidateClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOpenBakClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  private
    FOnGenerateSql: TNotifyEvent;
    FOnSaveBackupPath: TNotifyEvent;
    function GetBackupPath: string;
    function GetDbName: string;
    function GetSqlText: string;
    procedure SetBackupPath(const Value: string);
  public
    property SqlText: string read GetSqlText;
    property BackupPath: string read GetBackupPath write SetBackupPath;
    property DbName: string read GetDbName;
  published
    property OnGenerateSql: TNotifyEvent read FOnGenerateSql write FOnGenerateSql;
    property OnSaveBackupPath: TNotifyEvent read FOnSaveBackupPath write
        FOnSaveBackupPath;
  end;

implementation

{$R *.dfm}

uses
  FireDAC.Comp.Client,
  mDatas;

procedure TFrmRestore.FormCreate(Sender: TObject);
begin
  inherited;
  openDlg.Filter := 'Backup Files|*.bak|Tous|*.*';
end;

procedure TFrmRestore.FormShow(Sender: TObject);
var
  qry: TFDQuery;
begin
  inherited;
  qry := MyDatas.GetQuery(Self);
  qry.SQL.Add('SELECT '
              + '  SERVERPROPERTY(''InstanceDefaultBackupPath'') BackupPath,'
              + '  SERVERPROPERTY(''InstanceDefaultDataPath'') DataPath,'
              + '  SERVERPROPERTY(''InstanceDefaultLogPath'') LogPath '
              + 'FROM sys.dm_os_windows_info;');
  dsp1.DataSet := qry;
  cds1.Open;
end;

{$REGION 'Buttons'}
procedure TFrmRestore.btnValidateClick(Sender: TObject);
begin
  inherited;
  if Assigned(FOnGenerateSql) then
    FOnGenerateSql(Self);
end;

procedure TFrmRestore.btnOpenBakClick(Sender: TObject);
var
  str: string;
  qry: TFDQuery;
begin
  inherited;
  if (Trim(edtFile.Text) <> '') then
  begin
    str := ExtractFilePath(edtFile.Text);
    if DirectoryExists(str) then
      openDlg.InitialDir := str;
  end;

  if (openDlg.Execute) then
  begin
    qry := MyDatas.GetQuery(Self);
    qry.SQL.Text := 'RESTORE FILELISTONLY FROM DISK = :FILE;';
    qry.Params.ParamByName('FILE').Value := openDlg.FileName;
    dspBak.DataSet := qry;
    cdsBak.Open;

    edtFile.Text := openDlg.FileName;
    edtDbName.Text := ChangeFileExt(ExtractFileName(openDlg.FileName), '');
  end;
end;

procedure TFrmRestore.btnSaveClick(Sender: TObject);
begin
  inherited;
  if Assigned(FOnSaveBackupPath) then
    FOnSaveBackupPath(Self);
end;
{$ENDREGION}

{$REGION 'Getter/Setter'}
function TFrmRestore.GetBackupPath: string;
var
  str: string;
begin
  str := edtFile.Text;
  if (not str.Trim.IsEmpty) then
    Result := ExtractFilePath(str)
  else
    Result := string.Empty;
end;

function TFrmRestore.GetDbName: string;
begin
  Result := edtDbName.Text;
end;

function TFrmRestore.GetSqlText: string;
var
  lst: TStringList;
  dbName: string;
  dataName, logName,
  dataFile, logFile: string;
begin
  Result := '';

  if (Trim(edtFile.Text) = '')
    or (not FileExists(edtFile.Text))
    or (Trim(edtDbName.Text) = '') then
    Exit;

  if cdsBak.Locate('Type', 'D', []) then
    dataName := cdsBak['LogicalName']
  else
    dataName := '';

  if cdsBak.Locate('Type', 'L', []) then
    logName := cdsBak['LogicalName']
  else
    logName := '';
  cdsBak.First;

  if (dataName = '') or (logName = '') then
    Exit;

  lst := TStringList.Create;
  try
    dbName := Trim(edtDbName.Text);
    dbName := StringReplace(dbName, '''', '_', [rfReplaceAll]);

    dataFile := IncludeTrailingPathDelimiter(cds1['DataPath']);
    logFile := IncludeTrailingPathDelimiter(cds1['LogPath']);

    dataFile := Format('%s%s.mdf', [dataFile, dbName]);
    logFile := Format('%s%s_log.ldf', [logFile, dbName]);

    lst.Add('USE master;');
    lst.Add('GO');
    lst.Add(Format('IF EXISTS(SELECT database_id FROM sys.databases '
                   + 'WHERE NAME = N%s) BEGIN',
                   [QuotedStr(dbName)]));
    lst.Add(Format('    ALTER DATABASE [%s] SET SINGLE_USER '
                   + 'WITH ROLLBACK IMMEDIATE;',
                   [dbName]));
    lst.Add(Format('    DROP DATABASE [%s];', [dbName]));
    lst.Add('END;');
    lst.Add('GO');

    lst.Add(Format('RESTORE DATABASE [%s]', [dbName]));
    lst.Add(Format('FROM DISK = N%s WITH FILE = 1, NOUNLOAD, STATS = 5,',
                   [QuotedStr(edtFile.Text)]));
    lst.Add(Format('MOVE N%s ', [QuotedStr(dataName)]));
    lst.Add(Format('    TO N%s,', [QuotedStr(dataFile)]));
    lst.Add(Format('MOVE N%s', [QuotedStr(logName)]));
    lst.Add(Format('    TO N%s', [QuotedStr(logFile)]));
    lst.Add('GO');

    if (chkShrink.Checked) then
    begin
      lst.add(Format('DBCC SHRINKDATABASE(N%s);', [QuotedStr(dbName)]));
      lst.add('GO');
    end;
    Result := lst.Text;
  finally
    lst.Free;
  end;
end;

procedure TFrmRestore.SetBackupPath(const Value: string);
begin
  edtFile.Text := Value;
end;
{$ENDREGION}

end.
