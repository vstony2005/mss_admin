unit fScriptSql;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, fFormForTab, Vcl.StdCtrls, SynEdit,
  SynEditHighlighter, SynHighlighterSQL, Vcl.Buttons;

type
  TFrmScriptSql = class(TFrmForTab)
    SynSQLSyn1: TSynSQLSyn;
    edtRestore: TSynEdit;
    btnAdd: TSpeedButton;
    openDlg: TOpenDialog;
    btnRun: TSpeedButton;
    edtAfterRestore: TSynEdit;
    btnOpen: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
  private
    FDbName: string;
    procedure SetDbName(const Value: string);
    procedure SetRestoreSql(const Value: string);
    procedure AddFile(const AFile: string);
  public
    property DbName: string read FDbName write SetDbName;
    property RestoreSql: string write SetRestoreSql;
  end;

implementation

{$R *.dfm}

uses
  mDatas,
  FireDAC.Comp.Client;

procedure TFrmScriptSql.FormCreate(Sender: TObject);
begin
  inherited;
  openDlg.Filter := 'SQL Server files|*.sql|Tous|*.*';
end;

{$REGION 'Buttons'}
procedure TFrmScriptSql.btnAddClick(Sender: TObject);
begin
  inherited;
  if openDlg.Execute then
    AddFile(openDlg.FileName);
end;

procedure TFrmScriptSql.btnOpenClick(Sender: TObject);
begin
  inherited;
  if openDlg.Execute then
  begin
    edtAfterRestore.Clear;
    AddFile(openDlg.FileName);
  end;
end;

procedure TFrmScriptSql.btnRunClick(Sender: TObject);
var
  qry: TFDQuery;
  i: Integer;
  lst: TStringList;
  str: string;

  procedure AddEdit(AEdt: TSynEdit);
  var
    i: Integer;
  begin
    for i := 0 to AEdt.Lines.Count-1 do
    begin
      str := AEdt.Lines[i].Trim;

      if (str = string.Empty) or str.StartsWith('--') then
        Continue;

      lst.Add(str);
    end;
  end;

begin
  inherited;

  lst := TStringList.Create;
  try
    AddEdit(edtRestore);
    AddEdit(edtAfterRestore);

    if (lst.Text.IsEmpty) then
      Exit;


  if (MessageDlg('Would you like to run the SQL script?',
                 TMsgDlgType.mtConfirmation,
                 [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) = mrNo) then
    Exit;

    qry := MyDatas.GetQuery;
    try
      try
        for i := 0 to lst.Count-1 do
        begin
          str := lst[i].Trim;

          if (UpperCase(str) = 'GO') then
          begin
            if (not qry.SQL.Text.IsEmpty) then
              qry.ExecSQL;
            qry.SQL.Clear;
          end
          else
            qry.SQL.Add(str);
        end;

        if (not qry.SQL.Text.IsEmpty) then
          qry.ExecSQL;
      except
        on E: Exception do
          MessageDlg(Format('Error: %s', [e.Message]),
                     TMsgDlgType.mtError,
                     [TMsgDlgBtn.mbOK], 0);
      end;
    finally
      qry.Free;
    end;
  finally
    lst.Free;
  end;
end;
{$ENDREGION}

{$REGION 'Getter/Setter'}
procedure TFrmScriptSql.SetRestoreSql(const Value: string);
var
  lst: TStringList;
  i: Integer;
  str: string;
begin
  lst := TStringList.Create;
  try
    lst.Text := Value;

    if (lst.Count > 0) then
    begin
      edtRestore.Clear;

      edtRestore.Lines.Append('-- Restore DB');

      for i := 0 to lst.Count - 1 do
        edtRestore.Lines.Append(lst[i]);

      str := FormatDateTime('"-- Script generate on" mm/dd/yyyy "at" hh:nn', Now);
      edtRestore.Lines.Append('');
      edtRestore.Lines.Append(str);
    end
  finally
    lst.Free;
  end;
end;

procedure TFrmScriptSql.SetDbName(const Value: string);
begin
  if (not Value.Trim.IsEmpty) and (FDbName <> Value) then
  begin
    FDbName := Value;
    edtAfterRestore.Clear;
    AddFile(Format('%sScripts\%s.sql',
                   [ExtractFilePath(Application.ExeName),
                    FDbName]));
  end;
end;
{$ENDREGION}

procedure TFrmScriptSql.AddFile(const AFile: string);
var
  lst: TStringList;
  str: string;
begin
  if FileExists(AFile) then
  try
    lst := TStringList.Create;
    try
      lst.LoadFromFile(AFile);

      if (not edtAfterRestore.Lines.Text.IsEmpty) then
      begin
        edtAfterRestore.Lines.Add('');
        edtAfterRestore.Lines.Add('');
      end;

      edtAfterRestore.Lines.Add(Format('-- %s', [AFile]));
      for str in lst do
        edtAfterRestore.Lines.Add(str);
    finally
      lst.Free;
    end;
  except
    MessageDlg('Error to open file', TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;
end;

end.
