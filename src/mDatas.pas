unit mDatas;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL,
  FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  uServer;

type
  TDataModule1 = class(TDataModule)
    con1: TFDConnection;
  private
    function GetIsConnected: Boolean;
  public
    function ConnectMaster(const AServer: TServer): Boolean;
    function ConnectDatabase(const AServer: TServer; const ADatabase: string): Boolean;
    procedure CloseDatabase;

    function GetQuery(const AOwner: TComponent = nil): TFDQuery;

    function EncryptString(s: string): string;
    function DecryptString(s: string): string;

    property IsConnected: Boolean read GetIsConnected;
  end;

var
  MyDatas: TDataModule1;

implementation

uses
  Hash,
  DCPsha512, DCPsha1, DCPrc4, StrUtils, DCPrijndael, DCPblowfish;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


const
  PASSWORD1: string = 'passw1';
  PASSWORD2: string = 'passw2';

{ TDataModule1 }

function TDataModule1.ConnectMaster(const AServer: TServer): Boolean;
begin
  Result := ConnectDatabase(AServer, 'master');
end;

function TDataModule1.ConnectDatabase(const AServer: TServer;
  const ADatabase: string): Boolean;
begin
  Result := False;
  try
    if con1.Connected then
      con1.Close;
    con1.Params.Values['Server'] := AServer.Name;
    con1.Params.Database := ADatabase;
    con1.Params.UserName := AServer.User;
    con1.Params.Password := AServer.Password;
    con1.Connected := True;
    Result := True;
  except
  end;
end;

procedure TDataModule1.CloseDatabase;
begin
  con1.Close;
end;

function TDataModule1.GetIsConnected: Boolean;
begin
  Result := con1.Connected;
end;

function TDataModule1.GetQuery(const AOwner: TComponent = nil): TFDQuery;
var
  qry: TFDQuery;
begin
  Result := nil;
  if (con1.Connected) then
  begin
    qry := TFDQuery.Create(AOwner);
    qry.Connection := con1;
    Result := qry;
  end;
end;

function TDataModule1.EncryptString(s: string): string;
var
  blowFish: TDCP_Blowfish;
  aes: TDCP_rijndael;
  ret: string;
begin
  ret := string.Empty;

  aes := TDCP_rijndael.Create(Self);
  aes.InitStr(PASSWORD1, TDCP_sha512);

  blowFish := TDCP_Blowfish.Create(Self);
  blowFish.InitStr(PASSWORD2, TDCP_sha512);

  ret := AES.EncryptString(s);
  aes.Burn;
  aes.Free;

  Result   := Blowfish.EncryptString(ret);
  blowFish.Burn;
  blowFish.Free;
end;

function TDataModule1.DecryptString(s: string): string;
var
  blowFish: TDCP_Blowfish;
  aes: TDCP_rijndael;
  ret: string;
begin
  ret := string.Empty;

  aes := TDCP_rijndael.Create(Self);
  aes.InitStr(PASSWORD1, TDCP_sha512);

  blowFish := TDCP_Blowfish.Create(Self);
  blowFish.InitStr(PASSWORD2, TDCP_sha512);

  ret := Blowfish.DecryptString(s);
  blowFish.Burn;
  blowFish.Free;

  Result   := AES.DecryptString(ret);
  aes.Burn;
  aes.Free;
end;

end.
