unit uOptions;

interface

uses
  System.Generics.Collections,
  uServer;

type
  TOptions = class
  private
    FBackupPath: string;
    FServers: TObjectList<TServer>;
    function GetServer(AIndex: Integer): TServer;
    function GetCountDb: Integer;
    procedure SetBackupPath(const Value: string);
  public
    constructor Create;
    destructor Destroy; override;

    function WriteFile(AFilename: string): Boolean;
    function ReadFile(AFilename: string): Boolean;

    function AddServer(AServer: TServer): Integer;
    function RemoveServer(const AServer: TServer): Integer;
    procedure ClearServers;

    property BackupPath: string read FBackupPath write SetBackupPath;
    property Servers[index: Integer]: TServer read GetServer;
    property CountDb: Integer read GetCountDb;
  end;

implementation

uses
  System.JSON,
  System.IOUtils,
  System.SysUtils,
  Vcl.Dialogs,
  System.Classes,
  mDatas;

{ TOptions }

constructor TOptions.Create;
begin
  FServers := TObjectList<TServer>.Create;
  FServers.OwnsObjects := True;
end;

destructor TOptions.Destroy;
begin
  FServers.Free;

  inherited;
end;

function TOptions.AddServer(AServer: TServer): Integer;
begin
  Result := FServers.Add(AServer);
end;
function TOptions.RemoveServer(const AServer: TServer): Integer;
begin
  Result := FServers.Remove(AServer);
end;

procedure TOptions.ClearServers;
begin
  FServers.Clear;
end;

function TOptions.GetCountDb: Integer;
begin
  Result := FServers.Count;
end;

function TOptions.GetServer(AIndex: Integer): TServer;
begin
  Result := FServers.Items[AIndex];
end;

function TOptions.WriteFile(AFilename: string): Boolean;
begin
    Result := False;
    TThread.ForceQueue(nil,
    procedure
    var
      i: Integer;
      srv: TServer;
      str: string;
      dbs: TJSONArray;
      options, db: TJSONObject;
    begin
      try
        options := TJSONObject.Create;
        try
          dbs := TJSONArray.Create;
          options.AddPair('Databases', dbs);

          for i := 0 to FServers.Count-1 do
          begin

            srv := FServers[i];

            db := TJSONObject.Create;
            db.AddPair('Name', srv.Name);
            db.AddPair('User', srv.User);
            str := MyDatas.EncryptString(srv.Password);
            db.AddPair('Password', str);

            dbs.Add(db);
          end;

          options.AddPair('BackupPath', FBackupPath);

          TFile.WriteAllText(AFilename, options.ToJSON, TEncoding.UTF8);
        finally
          options.Free;
        end;
      except
        on e:Exception do
          ShowMessage(e.message);
      end;
    end);
end;

function TOptions.ReadFile(AFilename: string): Boolean;
var
  jsonfile, str: string;
  val: TJSONValue;
  opts, db: TJSONObject;
  dbs: TJSONArray;
  i: Integer;
  srv: TServer;
begin
  Result := False;
  if (not FileExists(AFilename)) then
    Exit;
  jsonfile := TFile.ReadAllText(AFilename);
  val := TJSONObject.ParseJSONValue(jsonfile);
  if not Assigned(val) then
    Exit;
  try
    FServers.Clear;
    opts := val as TJSONObject;
    dbs := opts.GetValue<TJSONArray>('Databases');
    for i := 0 to dbs.Count - 1 do
    begin
      db := dbs.Items[i] as TJSONObject;
      srv := TServer.Create;
      srv.Name := db.GetValue<string>('Name');
      srv.User := db.GetValue<string>('User');
      str := db.GetValue<string>('Password');
      str := MyDatas.DecryptString(str);
      srv.Password := str;
      FServers.Add(srv);
    end;

    if opts.TryGetValue<string>('BackupPath', str) then
      BackupPath := str;
  finally
    opts.Free;
  end;
end;

procedure TOptions.SetBackupPath(const Value: string);
begin
  if DirectoryExists(Value) then
    FBackupPath := Value;
end;

end.

