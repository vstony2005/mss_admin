unit uServer;

interface

type
  TServer = class
  private
    FPassword: string;
    FServerName: string;
    FUser: string;
  public
    property Name: string read FServerName write FServerName;
    property User: string read FUser write FUser;
    property Password: string read FPassword write FPassword;
  end;

implementation

end.

